
class CVehicle {
  // sử dụng được ở class con (subclass)
  protected String brand;
  protected int yearManufactured;
  // 
  public CVehicle() {
  }  
  // 
  public CVehicle(String paramBrand, int paramYearManufactured) {
	brand = paramBrand;
    yearManufactured = paramYearManufactured; 
  }
  // max 
  public void print() {
    System.out.println("CVehicle information... ");
  	System.out.println("brand: " + brand);
    System.out.println("year manufactured: " + yearManufactured);
  }
  //max speed of vehicle
  public int maxSpeedPerKm() {
	return 0; 
  };
  // 
  public void honk() {
    System.out.println("Tu, tu!");
  }
}

class CCar extends CVehicle {
  private String modelName;
  private String vid;
  //
  public CCar() {
  }
  //
  public CCar( String paramBrand, int paramYearManufactured, String paramModelName, String paramVid) {
	super(paramBrand, paramYearManufactured);
        modelName = paramModelName;
        vid = paramVid;
  }

  
  public static void main(String[] args) {
    CCar myFastCar = new CCar();
    myFastCCar.honk();
    System.out.println(myFastCCar.brand + " " + myFastCCar.modelName);
    System.out.println("Max speed: " +   String.valueOf(myFastCar.maxSpeedPerKm()) );
  
  }
}
